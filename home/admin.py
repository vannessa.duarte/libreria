from django.contrib import admin
from .models import *
# Register your models here.
class autorAdmin(admin.ModelAdmin):
    list_display = (
        'nombre',
        'nacionalidad',
        'id'
    )
    search_fields = ('nombre','nacionalidad')

admin.site.register(Autor,autorAdmin)
admin.site.register(Cliente)
admin.site.register(Libros)